/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isacsearchengine;

import java.util.Arrays;
import java.util.LinkedList;

/**
 *
 * @author xvybira5
 */
public class Query {
    private LinkedList<String> queryWords;
    
    public Query(String words) {
        queryWords = new LinkedList(Arrays.asList(words.split("\\s+")));
    }
    
    public LinkedList<String> words() {
        return queryWords;
    }
}
