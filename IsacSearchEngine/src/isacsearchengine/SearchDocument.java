/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isacsearchengine;

import java.util.*;
import java.util.stream.IntStream;

/**
 *
 * @author xvybira5
 */
public class SearchDocument {
    private LinkedList<String> words;
    private HashMap<String, Integer> frequencies;
    
    public SearchDocument(String document) {
        words = new LinkedList(Arrays.asList(document.split("\\s+")));
        frequencies = countFrequencies(words);
    }
    
    private HashMap<String, Integer> countFrequencies(List<String> words) {
        HashMap<String, Integer> result = new HashMap<>();
        
        words.forEach((String s) -> {
            if (result.containsKey(s)) {
                result.put(s, result.get(s) + 1); // the fuck immutables bruh
            } else {
                result.put(s, 1);
            }
        });
        
        return result;
    }
    
    public int countForWord(String word) {
        if (frequencies.containsKey(word)) {
            return frequencies.get(word);
        }
        // else
        return 0;
    }
    
    public boolean containsWord(String word) {
        if (frequencies.containsKey(word)) {
            return true;
        }
        // else
        return false;
    }
    
    @Override
    public String toString() {
        String result = new String();
        
        // Can run parallely
        /*
        words.forEach((String s) -> {
            result.concat(s.concat(" "));
        });
        */
        IntStream.range(0, 5 < words.size()? words.size() : words.size())
        .forEach(idx -> {
            result.concat(words.get(idx).concat(" "));
        });
        
        result.concat(".....");
        /*
        for (String word : words) {
            result.concat(word.concat(" "));
        }
        */
        
        return result;
    }
}
