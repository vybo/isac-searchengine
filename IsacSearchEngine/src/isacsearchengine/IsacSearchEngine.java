/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isacsearchengine;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author xvybira5
 */
public class IsacSearchEngine {

    static List<SearchDocument> documents;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length > 0) {
            try {
                documents = loadDocuments(args[0]);
            } catch (IOException ex) {
                Logger.getLogger(IsacSearchEngine.class.getName()).log(Level.SEVERE, null, ex);
                System.err.println("Error reading input file. Is the path correct?");
            }
        } else {
            System.err.println("Specify input file as an argument.");
        }
    }
    
    public static List<SearchDocument> loadDocuments(String file) throws IOException {
        List<SearchDocument> result = new ArrayList<>();
        
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
               result.add(new SearchDocument(line));
            }
        }
        
        return result;
    }
}
