/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isacsearchengine;

import java.util.*;

/**
 *
 * @author xvybira5
 */
public class SearchInstance {
    private Query query;
    private List<SearchDocument> documents;
    
    public SearchInstance(Query queryToSearch, List<SearchDocument> documentsToSearchIn) {
        query = queryToSearch;
        documents = documentsToSearchIn;
    }
    
    public SearchResult searchSharedWordCount() {
        HashMap<SearchDocument, Double> results = new HashMap<>();
        
        query.words().forEach(word -> { // Query for is first, because if we parallelize this, we can return suitable documents dynamically as they are found.
            documents.forEach(document -> {
                if (document.containsWord(word)) {
                    results.put(document, results.get(document) + 1d); // the fuck immutables bruh
                } else {
                    results.put(document, 0.0d);
                }
            });
        });
        HashMap<SearchDocument, Double> test = MapUtil.sortByValue((Map<K, V>) results);
        return new SearchResult(query, test);
    }
}
